ROS : Writing a Simple Publisher and Subscriber (Python)
========================================================

### 话题编程

![](_image/ROS_Install_4.png)

**如何自定义话题消息：**

-   定义 msg 文件
-   在 package.xml 中添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**话题编程流程：**

-   创建发布者
    -   初始化 ROS 节点
    -   向 ROS Master 注册节点信息，包括发布的话题名和话题中的消息类型
    -   按照一定频率循环发布消息
-   创建订阅者
    -   初始化 ROS 节点
    -   订阅需要的话题
    -   循环等待话题消息，接收到消息后进入回调函数
    -   在回调函数中完成消息处理
-   运行可执行程序

**下面实现一个简单的例子，发布者每 100ms 发布实时的时间戳，订阅者实时监听发布者发布的内容。**

新建 msg 消息目录，定义 msg 消息：

``` {.sourceCode .bash}
lan@lan-vm:~$ roscd beginner_tutorials/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ mkdir msg
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd msg/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ touch Num.msg
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ rosed beginner_tutorials Num.msg
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ cat Num.msg
int64 num
```

编辑 package.xml 文件，增加依赖：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ rosed beginner_tutorials package.xml
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ tail -n +62 ../package.xml | head -n 2
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```

编辑 CMakeLists.txt 文件：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ rosed beginner_tutorials CMakeLists.txt
```

-   在 CMakeLists.txt 文件中增加对 message\_generation 的依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ tail -n +10 ../CMakeLists.txt | head -n 6
    find_package(catkin REQUIRED COMPONENTS
      roscpp
      rospy
      std_msgs
      message_generation
    )
    ```

-   增加消息文件

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ tail -n +50 ../CMakeLists.txt | head -n 7
    ## Generate messages in the 'msg' folder
    add_message_files(
      FILES
      Num.msg
    #   Message1.msg
    #   Message2.msg
    )
    ```

-   增加消息生成包

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ tail -n +107 ../CMakeLists.txt | head -n 6
    catkin_package(
    #  INCLUDE_DIRS include
    #  LIBRARIES beginner_tutorials
      CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
    #  DEPENDS system_lib
    )
    ```

-   添加包含 msgs 的软件包依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ tail -n +72 ../CMakeLists.txt | head -n 5
    ## Generate added messages and services with any dependencies listed here
    generate_messages(
      DEPENDENCIES
      std_msgs
    )
    ```

编译代码：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/msg$ cd ~/catkin_ws
lan@lan-vm:~/catkin_ws$ catkin_make
```

检查服务：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws$ rosmsg show beginner_tutorials/Num
int64 num
```

创建发布者：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws$ roscd beginner_tutorials
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ mkdir scripts
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd scripts/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch talker.py

lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x talker.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials talker.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cat talker.py
#!/usr/bin/env python
#coding=utf-8

#import roslib;roslib.load_manifest('beginner_tutorials')
import rospy
from beginner_tutorials.msg import Num

def talker():
    pub = rospy.Publisher('chatter', Num, queue_size = 10)
    rospy.init_node('talker', anonymous = True)
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        mynum = int(rospy.get_time())
        msg = Num()
        msg.num = mynum
        rospy.loginfo(mynum)
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
```

创建订阅者：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch listener.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x listener.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials listener.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cat listener.py
#!/usr/bin/env python
#coding=utf-8


import rospy
from beginner_tutorials.msg import Num

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %d', data.num)

def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', Num, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
```

运行代码：

-   打开终端一，启动 ros master ：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roscore
    ```

-   打开终端二，创建发布者：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials talker.py
    [INFO] [1566976295.506557]: 1566976295
    [INFO] [1566976295.606966]: 1566976295
    [INFO] [1566976295.706854]: 1566976295
    [INFO] [1566976295.806853]: 1566976295
    [INFO] [1566976295.906801]: 1566976295
    [INFO] [1566976296.006810]: 1566976296
    [INFO] [1566976296.106888]: 1566976296
    [INFO] [1566976296.206844]: 1566976296
    [INFO] [1566976296.306871]: 1566976296
    [INFO] [1566976296.406712]: 1566976296
    [INFO] [1566976296.506850]: 1566976296
    [INFO] [1566976296.606913]: 1566976296
    [INFO] [1566976296.706885]: 1566976296
    [INFO] [1566976296.806945]: 1566976296
    [INFO] [1566976296.906730]: 1566976296
    [INFO] [1566976297.006825]: 1566976297
    [INFO] [1566976297.106858]: 1566976297
    [INFO] [1566976297.206864]: 1566976297
    ```

-   打开终端三，创建订阅者：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials listener.py
    [INFO] [1566976295.507102]: /listener_20307_1566976294990I heard 1566976295
    [INFO] [1566976295.607586]: /listener_20307_1566976294990I heard 1566976295
    [INFO] [1566976295.707530]: /listener_20307_1566976294990I heard 1566976295
    [INFO] [1566976295.807433]: /listener_20307_1566976294990I heard 1566976295
    [INFO] [1566976295.907351]: /listener_20307_1566976294990I heard 1566976295
    [INFO] [1566976296.007628]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.107569]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.207443]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.307454]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.407328]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.507578]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.607614]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.707570]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.807635]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976296.907325]: /listener_20307_1566976294990I heard 1566976296
    [INFO] [1566976297.007538]: /listener_20307_1566976294990I heard 1566976297
    [INFO] [1566976297.107441]: /listener_20307_1566976294990I heard 1566976297
    [INFO] [1566976297.207577]: /listener_20307_1566976294990I heard 1566976297
    ```

**制作 Launch 文件 ：**

-   在 beginner\_tutorials 目录下新建 bringup 目录，进入 bringup ，新建
    talker-and-listener.launch 并编辑

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roscd beginner_tutorials
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ mkdir -p bringup
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd bringup/
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ touch talker-and-listener.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ rosed beginner_tutorials talker-and-listener.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ cat talker-and-listener.launch
    <launch>
        <node name="talker" pkg="beginner_tutorials" type="talker.py" />
        <node name="listener" pkg="beginner_tutorials" type="listener.py" />
    </launch>
    ```

-   运行 launch

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ roslaunch beginner_tutorials talker-and-listener.launch
    ... logging to /home/lan/.ros/log/9aae9364-c982-11e9-b22e-000c29d2e743/roslaunch-lan-virtual-machine-10516.log
    Checking log directory for disk usage. This may take awhile.
    Press Ctrl-C to interrupt
    Done checking log file disk usage. Usage is <1GB.

    started roslaunch server http://lan-virtual-machine:44175/

    SUMMARY
    ========

    PARAMETERS
     * /rosdistro: kinetic
     * /rosversion: 1.12.14

    NODES
      /
        listener (beginner_tutorials/listener.py)
        talker (beginner_tutorials/talker.py)

    auto-starting new master
    process[master]: started with pid [10527]
    ROS_MASTER_URI=http://localhost:11311

    setting /run_id to 9aae9364-c982-11e9-b22e-000c29d2e743
    process[rosout-1]: started with pid [10540]
    started core service [/rosout]
    process[talker-2]: started with pid [10543]
    process[listener-3]: started with pid [10544]
    ```

-   使用 `rqt_console` 命令查看日志输出

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rqt_console
    ```

    ![](_image/ROS_WritingPublisherSubscriber_Python_1.png)
