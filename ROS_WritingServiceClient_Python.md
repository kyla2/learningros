ROS : Writing a Simple Service and Client (Python)
==================================================

### 服务编程

![](_image/ROS_Install_5.png)

**服务编程流程：**

-   创建服务器
    -   初始化 ROS 节点
    -   创建 Server 实例
    -   循环等待服务请求，进入回调函数
    -   在回调函数中完成服务功能的处理，并反馈应答数据
-   创建客户端
    -   初始化 ROS 节点
    -   创建 Client 实例
    -   发布服务请求数据
    -   等待 Server 处理之后的应答结果
-   运行可执行程序

**如何自定义服务请求与应答：**

-   定义 srv 文件
-   在 package.xml 添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**下面的例子是创建一个服务节点 (add\_two\_ints\_server) ，接收两个 Int 类型的数据并返回它们的和。**

在已经创建功能包的情况下，在一个新终端中输入如下命令可以进入指定功能包的路径：

``` {.sourceCode .bash}
lan@lan-vm:~$ roscd beginner_tutorials
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ pwd
/home/lan/catkin_ws/src/beginner_tutorials
```

定义
[srv](https://github.com/ros/ros_tutorials/blob/melodic-devel/rospy_tutorials/srv/AddTwoInts.srv)
文件：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ ls
bringup  CMakeLists.txt  include  msg  package.xml  scripts  src
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ mkdir srv
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd srv/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ gedit AddTwoInts.srv
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ cat AddTwoInts.srv
int64 a
int64 b
---
int64 sum
```

编辑 CMakeLists.txt 文件：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ rosed beginner_tutorials CMakeLists.txt
```

-   在 CMakeLists.txt 文件中增加对 message\_generation 的依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ tail -n +10 ../CMakeLists.txt | head -n 6
    find_package(catkin REQUIRED COMPONENTS
      roscpp
      rospy
      std_msgs
      message_generation
    )
    ```

-   增加 srv 文件

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ tail -n +58 ../CMakeLists.txt | head -n 7
    ## Generate services in the 'srv' folder
    add_service_files(
      FILES
      AddTwoInts.srv
    #   Service1.srv
    #   Service2.srv
    )
    ```

-   增加消息生成包

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ tail -n +107 ../CMakeLists.txt | head -n 7
    ## DEPENDS: system dependencies of this project that dependent projects also need
    catkin_package(
    #  INCLUDE_DIRS include
    #  LIBRARIES beginner_tutorials
      CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
    #  DEPENDS system_lib
    )
    ```

-   添加包含 msgs 的软件包依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ tail -n +73 ../CMakeLists.txt | head -n 5
    ## Generate added messages and services with any dependencies listed here
    generate_messages(
      DEPENDENCIES
      std_msgs
    )
    ```

-   通过 `rossrv show` 命令，检查 ROS 是否能够识该服务

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/srv$ rossrv show beginner_tutorials/AddTwoInts
    int64 a
    int64 b
    ---
    int64 sum
    ```

编译代码：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws$ cd ~/catkin_ws
lan@lan-vm:~/catkin_ws$ catkin_make
```

创建服务端：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws$ roscd beginner_tutorials
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ ls
bringup  CMakeLists.txt  include  msg  package.xml  scripts  src  srv
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd scripts/

lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch add_two_ints_server.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials add_two_ints_server.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x add_two_ints_server.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cat add_two_ints_server.py
#!/usr/bin/env python

from beginner_tutorials.srv import AddTwoInts, AddTwoIntsResponse  # import two classes ("AddTwoInts, AddTwoIntsResponse") from _AddTwoInts.py
import rospy

def handle_add_two_ints(req):
    print "Returning [%s + %s = %s]"%(req.a, req.b, (req.a + req.b))
    return AddTwoIntsResponse(req.a + req.b)

def add_two_ints_server():
    rospy.init_node('add_two_ints_server')  # Initializing the ROS Node, named 'add_two_ints_server'
    # This declares a new service named add_two_ints with the AddTwoInts service type. All requests are passed to handle_add_two_ints function.
    # handle_add_two_ints is called with instances of AddTwoIntsRequest and returns instances of AddTwoIntsResponse.
    s = rospy.Service('add_two_ints', AddTwoInts, handle_add_two_ints)
    print "Ready to add two ints."
    rospy.spin()  # spin() simply keeps python from exiting until this node is stopped.

if __name__ == "__main__":
    add_two_ints_server()
```

创建客户端：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch add_two_ints_client.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials add_two_ints_client.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x add_two_ints_client.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cat add_two_ints_client.py
#!/usr/bin/env python

import sys
import rospy
from beginner_tutorials.srv import *  # import from _AddTwoInts.py

def add_two_ints_client(x, y):
    rospy.wait_for_service('add_two_ints')  # This is a convenience method that blocks until the service named add_two_ints is available
    try:
        add_two_ints = rospy.ServiceProxy('add_two_ints', AddTwoInts)  # create a handle for calling the service
        resp1 = add_two_ints(x, y)  # use this handle just like a normal function and call it
        return resp1.sum
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def usage():
    return "%s [x y]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 3:
        x = int(sys.argv[1])
        y = int(sys.argv[2])
    else:
        print usage()
        sys.exit(1)
    print "Requesting %s + %s" % (x, y)
    print "%s + %s = %s" % (x, y, add_two_ints_client(x, y))
```

编译代码：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cd ~/catkin_ws
lan@lan-vm:~/catkin_ws$ catkin_make
```

运行代码：

-   打开终端一，启动 ros master ：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roscore
    ```

-   打开终端二，创建服务端：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_server.py
    Ready to add two ints.
    ```

-   打开终端三，创建客户端：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_client.py
    /home/lan/catkin_ws/src/beginner_tutorials/scripts/add_two_ints_client.py [x y]

    lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_client.py 4 5
    Requesting 4 + 5
    4 + 5 = 9
    ```

-   服务端输出结果：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_server.py
    Ready to add two ints.
    Returning [4 + 5 = 9]
    ```

**制作 Launch 文件 ：**

-   在 beginner\_tutorials 目录下新建 bringup 目录，进入 bringup ，新建
    service-and-client.launch 并编辑

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws$ roscd beginner_tutorials
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ ls
    bringup  CMakeLists.txt  include  msg  package.xml  scripts  src  srv
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd bringup/
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ touch service-and-client.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ rosed beginner_tutorials service-and-client.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ cat service-and-client.launch
    <launch>
        <node name="add_two_ints_server" pkg="beginner_tutorials" type="add_two_ints_server.py" />
    </launch>
    ```

-   运行 launch

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ roslaunch beginner_tutorials service-and-client.launch
    ... logging to /home/lan/.ros/log/8bbdad8e-cd58-11e9-8142-000c29d2e743/roslaunch-lan-virtual-machine-15737.log
    Checking log directory for disk usage. This may take awhile.
    Press Ctrl-C to interrupt
    Done checking log file disk usage. Usage is <1GB.

    started roslaunch server http://lan-virtual-machine:41553/

    SUMMARY
    ========

    PARAMETERS
     * /rosdistro: kinetic
     * /rosversion: 1.12.14

    NODES
      /
        add_two_ints_server (beginner_tutorials/add_two_ints_server.py)

    auto-starting new master
    process[master]: started with pid [15747]
    ROS_MASTER_URI=http://localhost:11311

    setting /run_id to 8bbdad8e-cd58-11e9-8142-000c29d2e743
    process[rosout-1]: started with pid [15760]
    started core service [/rosout]
    process[add_two_ints_server-2]: started with pid [15768]
    ```

    在另一个终端上运行客户端节点：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_client.py 4 9
    Requesting 4 + 9
    4 + 9 = 13
    ```


