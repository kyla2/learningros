#! /usr/bin/env python

import rospy
import actionlib
import beginner_tutorials.msg

def fibonacci_client():
    client = actionlib.SimpleActionClient('fibonacci', beginner_tutorials.msg.FibonacciAction)

    client.wait_for_server()

    goal = beginner_tutorials.msg.FibonacciGoal(order=20)

    client.send_goal(goal)

    client.wait_for_result()

    return client.get_result()  # A FibonacciResult

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can publish and subscribe over ROS.
        rospy.init_node('fibonacci_client_py')
        result = fibonacci_client()
        print("Result:", ', '.join([str(n) for n in result.sequence]))
    except rospy.ROSInterruptException:
        print("program interrupted before completion")

