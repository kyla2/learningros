ROS : 通信编程 (Python)
=======================

> 2019-09-07 新增： ROS 通信编程 (Python) 文档，包括：话题编程、服务编程、动作编程。

概述
----

这里实现了 ROS WIKI 通信编程中 Python 的例子，包括：

-   话题编程：发布者每 100ms
    发布实时的时间戳，订阅者实时监听发布者发布的内容
-   服务编程：创建一个服务节点 (add\_two\_ints\_server) ，接收两个 Int
    类型的数据并返回它们的和
-   动作编程：创建 Fibonacci （斐波那契数列）动作服务端，计算 Fibonacci
    数列的顺序，得到计算后的数列，创建 Fibonacci
    （斐波那契数列）动作客户端，发送目标并获取反馈结果

本项目需要先创建 catkin\_ws 工作空间和 beginner\_tutorials
功能包，具体操作见：

-   [Creating a workspace for
    catkin](http://wiki.ros.org/catkin/Tutorials/create_a_workspace)
-   [Creating a ROS
    Package](http://wiki.ros.org/ROS/Tutorials/CreatingPackage)

beginner\_tutorials 功能包的目录结构及说明如下：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ tree
.
├── action        # 动作编程中定义的 action 文件（行为规范）
│   ├── DoDishes.action
│   └── Fibonacci.action
├── bringup        # 定义的 Launch 文件，可以通过 XML 文件实现多节点的配置和启动，可自动启动 ROS Master
│   ├── action-service-and-client.launch
│   ├── service-and-client.launch
│   └── talker-and-listener.launch
├── CMakeLists.txt
├── include
│   └── beginner_tutorials
├── msg        # 话题编程中定义的 msg 文件
│   └── Num.msg
├── package.xml
├── scripts        # Python 源码
│   ├── add_two_ints_client.py        # 服务编程的客户端
│   ├── add_two_ints_server.py        # 服务编程的服务端
│   ├── fibonacci_client.py           # 动作编程的客户端
│   ├── fibonacci_server.py           # 动作编程的服务端
│   ├── listener.py                   # 话题编程的订阅者
│   └── talker.py                     # 话题编程的发布者
├── src        # C++ 源码
└── srv        # 服务编程中定义的 srv 文件
    └── AddTwoInts.srv

8 directories, 15 files
```

话题编程
--------

![](_image/ROS_Install_4.png)

**如何自定义话题消息：**

-   定义 msg 文件
-   在 package.xml 中添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**话题编程流程：**

-   创建发布者
    -   初始化 ROS 节点
    -   向 ROS Master 注册节点信息，包括发布的话题名和话题中的消息类型
    -   按照一定频率循环发布消息
-   创建订阅者
    -   初始化 ROS 节点
    -   订阅需要的话题
    -   循环等待话题消息，接收到消息后进入回调函数
    -   在回调函数中完成消息处理
-   运行可执行程序
    -   打开终端一，启动 ros master ：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ roscore
        ```

    -   打开终端二，创建发布者：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ rosrun beginner_tutorials talker.py
        ```

    -   打开终端三，创建订阅者：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ rosrun beginner_tutorials listener.py
        ```

[话题编程 Demo 实现](ROS_WritingPublisherSubscriber_Python.md)

服务编程
--------

![](_image/ROS_Install_5.png)

**如何自定义服务请求与应答：**

-   定义 srv 文件
-   在 package.xml 添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**服务编程流程：**

-   创建服务器
    -   初始化 ROS 节点
    -   创建 Server 实例
    -   循环等待服务请求，进入回调函数
    -   在回调函数中完成服务功能的处理，并反馈应答数据
-   创建客户端
    -   初始化 ROS 节点
    -   创建 Client 实例
    -   发布服务请求数据
    -   等待 Server 处理之后的应答结果
-   运行可执行程序
    -   打开终端一，启动 ros master ：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ roscore
        ```

    -   打开终端二，创建服务端：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_server.py
        ```

    -   打开终端三，创建客户端：

        ``` {.sourceCode .bash}
        lan@lan-vm:~$ rosrun beginner_tutorials add_two_ints_client.py 4 5
        ```

[服务编程 Demo 实现](ROS_WritingServiceClient_Python.md)

动作编程
--------

什么是动作：

-   一种问答通信机制
-   带有连续反馈
-   可以在任务过程中止运行
-   基于 ROS 的消息机制实现

ActionClient 和 ActionServer 通过" ROS 行为协议"进行通信，该协议是建立在
ROS 消息之上的。然后，客户机和服务器为用户提供一个简单的 API
，用于请求目标(在客户端)或通过函数调用和回调执行目标(在服务器端)。

![](_image/ROS_Install_6.png)

Action 的接口：

-   goal 发布任务目标
-   cancel 请求取消任务
-   status 通知客户端当前的状态
-   feedback 周期反馈任务运行的监控数据
-   result 向客户端发送任务的执行结果，只发布一次

![](_image/ROS_Install_7.png)

**如何自定义动作消息：**

-   定义 action 文件（行为规范）
-   在 package.xml 添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**如何实现一个动作服务器：**

-   初始化 ROS 节点
-   创建动作服务器实例
-   启动服务器，等待动作请求
-   在回调函数中完成动作服务功能的处理，并反馈进度信息
-   动作完成，发送结束信息

**如何实现一个动作客户端：**

-   初始化 ROS 节点
-   创建动作客户端实例
-   连接动作服务端
-   发送动作目标
-   根据不同类型的服务端反馈处理回调函数

**如何编译代码：**

-   设置需要编译的代码和生成的可执行文件
-   设置链接库
-   设置依赖

**运行可执行程序：**

-   打开终端一，启动 ros master ：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roscore
    ```

-   打开终端二，创建动作服务端：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials fibonacci_server.py
    ```

-   打开终端三，创建动作客户端：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials fibonacci_client.py
    ```

[动作编程 Demo 实现](ROS_WritingActionServiceClient_Python.md)
