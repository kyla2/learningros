ROS : Writing a Simple Action Service and Client (Python)
=========================================================

### 动作编程

什么是动作：

-   一种问答通信机制
-   带有连续反馈
-   可以在任务过程中止运行
-   基于 ROS 的消息机制实现

ActionClient 和 ActionServer 通过" ROS 行为协议"进行通信，该协议是建立在
ROS 消息之上的。然后，客户机和服务器为用户提供一个简单的 API
，用于请求目标(在客户端)或通过函数调用和回调执行目标(在服务器端)。

![](_image/ROS_Install_6.png)

Action 的接口：

-   goal 发布任务目标
-   cancel 请求取消任务
-   status 通知客户端当前的状态
-   feedback 周期反馈任务运行的监控数据
-   result 向客户端发送任务的执行结果，只发布一次

![](_image/ROS_Install_7.png)

**如何自定义动作消息：**

-   定义 action 文件（行为规范）
-   在 package.xml 添加功能包依赖
-   在 CMakeLists.txt 添加编译选项

**如何实现一个动作服务器：**

-   初始化 ROS 节点
-   创建动作服务器实例
-   启动服务器，等待动作请求
-   在回调函数中完成动作服务功能的处理，并反馈进度信息
-   动作完成，发送结束信息

**如何实现一个动作客户端：**

-   初始化 ROS 节点
-   创建动作客户端实例
-   连接动作服务端
-   发送动作目标
-   根据不同类型的服务端反馈处理回调函数

**如何编译代码：**

-   设置需要编译的代码和生成的可执行文件
-   设置链接库
-   设置依赖

**下面实现一个简单的例子，创建 Fibonacci （斐波那契数列）动作服务端，计算 Fibonacci 数列的顺序，得到计算后的数列，创建 Fibonacci （斐波那契数列）动作客户端，发送目标并获取反馈结果。**

在已经创建功能包的情况下，在一个新终端中输入如下命令可以进入指定功能包的路径：

``` {.sourceCode .bash}
lan@lan-vm:~$ roscd beginner_tutorials
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ pwd
/home/lan/catkin_ws/src/beginner_tutorials
```

定义 action 文件（行为规范），这个文件定义目标、结果和行为的反馈话题的类型和文本格式：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ ls
bringup  CMakeLists.txt  include  msg  package.xml  scripts  src  srv
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ mkdir action
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd action/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ touch Fibonacci.action
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ rosed beginner_tutorials Fibonacci.action
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ cat Fibonacci.action
# goal definition
int32 order
---
# result definition
int32[] sequence
---
# feedback
int32[] sequence
```

编辑 CMakeLists.txt 文件：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ rosed beginner_tutorials CMakeLists.txt

# # 主要修改为下面的内容
# find_package(catkin REQUIRED genmsg actionlib_msgs actionlib)
# add_action_files(DIRECTORY action FILES Fibonacci.action)
# generate_messages(DEPENDENCIES actionlib_msgs)
```

-   在 CMakeLists.txt 文件中增加对 message\_generation 和
    actionlib\_msgs 、 actionlib 的依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ tail -n +10 ../CMakeLists.txt | head -n 8
    find_package(catkin REQUIRED COMPONENTS
      roscpp
      rospy
      std_msgs
      message_generation
      actionlib_msgs
      actionlib
    )
    ```

-   增加 action 文件

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ tail -n +68 ../CMakeLists.txt | head -n 7
    ## Generate actions in the 'action' folder
    add_action_files(
      FILES
      Fibonacci.action
    #  Action1.action
    #  Action2.action
    )
    ```

-   添加包含 msgs 的软件包依赖

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ tail -n +76 ../CMakeLists.txt | head -n 6
    ## Generate added messages and services with any dependencies listed here
    generate_messages(
      DEPENDENCIES
      std_msgs
      actionlib_msgs
    )
    ```

-   增加消息生成包

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ tail -n +111 ../CMakeLists.txt | head -n 7
    ## DEPENDS: system dependencies of this project that dependent projects also need
    catkin_package(
    #  INCLUDE_DIRS include
    #  LIBRARIES beginner_tutorials
      CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
    #  DEPENDS system_lib
    )
    ```

编辑 package.xml 文件：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ rosed beginner_tutorials package.xml
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ tail -n +65 ../package.xml | head -n 4
  <build_depend>actionlib</build_depend>
  <build_depend>actionlib_msgs</build_depend>
  <exec_depend>actionlib</exec_depend>
  <exec_depend>actionlib_msgs</exec_depend>
```

编译代码：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/action$ cd ~/catkin_ws/
lan@lan-vm:~/catkin_ws$ catkin_make
```

使用 simple\_action\_server 库创建 Python 的 Fibonacci （斐波那契数列） action 服务端，计算 Fibonacci 数列的顺序，得到计算后的数列。

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws$ roscd beginner_tutorials/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd scripts/
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch fibonacci_server.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials fibonacci_server.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x fibonacci_server.py
```

fibonacci\_server.py 代码及详细注释如下， **但在功能包中的源码不能出现中文注释，否则执行报错** ：

``` {.sourceCode .python}
#! /usr/bin/env python

import rospy
import actionlib  # 加载 actionlib 库用于实现简单行为
import beginner_tutorials.msg  # 导入生成的消息， action 会生成用于发送目标，接受反馈的消息

class FibonacciAction(object):
    # 创建用于发布反馈 (feedback) / 结果 (result) 的消息
    _feedback = beginner_tutorials.msg.FibonacciFeedback()
    _result = beginner_tutorials.msg.FibonacciResult()

    def __init__(self, name):
        self._action_name = name
        # SimpleActionServer 创建服务端，需 4 个参数： action_name 、 action type 、 callback 函数（可选）、 auto_start
        self._as = actionlib.SimpleActionServer(self._action_name, beginner_tutorials.msg.FibonacciAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()

    # 定义回调函数，当新目标到达， rospy.loginfo 输出相关信息，可知道正在执行的 action
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)  # Convenience class for sleeping in a loop at a specified rate (1hz)
        success = True

        # 附加种子用于 fibonacci 序列
        self._feedback.sequence = []
        self._feedback.sequence.append(0)
        self._feedback.sequence.append(1)

        # 发布信息到控制台，用于用户查看
        rospy.loginfo('%s: Executing, creating fibonacci sequence of order %i with seeds %i, %i' % (self._action_name, goal.order, self._feedback.sequence[0], self._feedback.sequence[1]))

        # 开始运行行为
        for i in range(1, goal.order):
            # check that preempt has not been requested by the client
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
                break
            self._feedback.sequence.append(self._feedback.sequence[i] + self._feedback.sequence[i-1])  # Fibonacci 数列放入反馈变量里
            # 发布反馈(feedback)
            self._as.publish_feedback(self._feedback)
            # 这个步骤不是必要的，用于演示效果所以序列会以 1Hz 完成
            r.sleep()

        # 当服务器端完成计算，调用 set_succeeded 通知客户端，目标已经完成
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)

if __name__ == '__main__':
    rospy.init_node('fibonacci')  # 初始化节点，节点名字是 fibonacci
    server = FibonacciAction(rospy.get_name())
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
```

使用 SimpleActionClient 库创建 Python 的 Fibonacci （斐波那契数列）
action 客户端，发送目标并获取反馈结果。

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ touch fibonacci_client.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ rosed beginner_tutorials fibonacci_client.py
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ chmod +x fibonacci_client.py
```

fibonacci\_client.py 代码及详细注释如下， **但在功能包中的源码不能出现中文注释，否则执行报错** ：

``` {.sourceCode .python}
#! /usr/bin/env python

import rospy
import actionlib  # 加载 SimpleActionClient
import beginner_tutorials.msg  # 加载 fibonacci 行为消息，包含目标消息和结果消息

def fibonacci_client():
    # 创建 SimpleActionClient ，传递行为类型 (FibonacciAction) 到构造函数。
    client = actionlib.SimpleActionClient('fibonacci', beginner_tutorials.msg.FibonacciAction)

    # 等待，直到行为服务器挂起或运行。
    # 监听目标。
    client.wait_for_server()

    # 创建一个目标发送到行为服务器。
    goal = beginner_tutorials.msg.FibonacciGoal(order=20)

    # 发送目标到行为服务器。
    client.send_goal(goal)

    # 等待服务器完成处理行为。
    client.wait_for_result()

    # 打印运行行为结果。
    return client.get_result()  # A FibonacciResult

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can publish and subscribe over ROS.
        rospy.init_node('fibonacci_client_py')
        result = fibonacci_client()
        print("Result:", ', '.join([str(n) for n in result.sequence]))
    except rospy.ROSInterruptException:
        print("program interrupted before completion")
```

编译代码：

``` {.sourceCode .bash}
lan@lan-vm:~/catkin_ws/src/beginner_tutorials/scripts$ cd ~/catkin_ws/
lan@lan-vm:~/catkin_ws$ catkin_make
```

运行代码：

-   打开终端一，启动 ros master ：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roscore
    ```

-   打开终端二，创建动作服务端（打开动作客户端后运行的结果如下）：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials fibonacci_server.py
    [INFO] [1567491471.521943]: /fibonacci: Executing, creating fibonacci sequence of order 20 with seeds 0, 1
    [INFO] [1567491490.523182]: /fibonacci: Succeeded
    ```

-   打开终端三，创建动作客户端：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials fibonacci_client.py
    ('Result:', '0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765')
    ```

**制作 Launch 文件 ：**

-   在 beginner\_tutorials 目录下新建 bringup 目录，进入 bringup ，新建
    action-service-and-client.launch 并编辑

    ``` {.sourceCode .bash}
    lan@lan-vm:~/catkin_ws$ roscd beginner_tutorials
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ ls
    action  bringup  CMakeLists.txt  include  msg  package.xml  scripts  src  srv
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials$ cd bringup/
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ touch action-service-and-client.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ rosed beginner_tutorials action-service-and-client.launch
    lan@lan-vm:~/catkin_ws/src/beginner_tutorials/bringup$ cat action-service-and-client.launch
    <launch>
        <node name="fibonacci" pkg="beginner_tutorials" type="fibonacci_server.py" />
    </launch>
    ```

-   在新终端运行 launch 文件

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ roslaunch beginner_tutorials action-service-and-client.launch
    ... logging to /home/lan/.ros/log/c77851c4-ce13-11e9-8142-000c29d2e743/roslaunch-lan-virtual-machine-31777.log
    Checking log directory for disk usage. This may take awhile.
    Press Ctrl-C to interrupt
    Done checking log file disk usage. Usage is <1GB.

    started roslaunch server http://lan-virtual-machine:32811/

    SUMMARY
    ========

    PARAMETERS
     * /rosdistro: kinetic
     * /rosversion: 1.12.14

    NODES
      /
        fibonacci (beginner_tutorials/fibonacci_server.py)

    auto-starting new master
    process[master]: started with pid [31787]
    ROS_MASTER_URI=http://localhost:11311

    setting /run_id to c77851c4-ce13-11e9-8142-000c29d2e743
    process[rosout-1]: started with pid [31800]
    started core service [/rosout]
    process[fibonacci-2]: started with pid [31804]
    ```

    在另一个终端上运行客户端节点：

    ``` {.sourceCode .bash}
    lan@lan-vm:~$ rosrun beginner_tutorials fibonacci_client.py
    ('Result:', '0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765')
    ```


